package cover;
//Justyna Micota 418427

import java.util.ArrayList;
import java.util.Arrays;

// representation of a problem and solutions development through boolean[] covered
public class Question {
    private final int bound;
    private boolean[] covered;

    public Question(int bound) {
        this.bound = bound;
        this.covered = new boolean[bound + 1];
        resetCovered();
    }

    public void resetCovered() {
        Arrays.fill(covered, false);
        covered[0] = true;
    }

    public boolean isCovered() {
        for(int i = 1; i <= bound; i++) {
            if (!covered[i]) {
                return false;
            }
        }
        return true;
    }

    private boolean isSingleton(SingleSet set) {
        return (set.nrOfElements() == 1);
    }

    private boolean isFinite(SingleSet set) {
        return (set.nrOfElements() > 1);
    }

    private boolean correctNumber(int x) {
        return((x > 0 && x <= bound));
    }

    private void coverWithSingleSet(SingleSet set, boolean[] toCover) {
        if (isSingleton(set)) {
            if (correctNumber(set.getBegin())) {
                toCover[set.getBegin()] = true;
            }
        }
        else {
            int begin = set.getBegin();
            int dif = set.getDifference();
            int end = bound;
            if (isFinite(set) && set.getEnd() < bound) {
                end = set.getEnd();
            }
            for (int i = begin; i <= end; i += dif) {
                if (correctNumber(i)) {
                    toCover[i] = true;
                }
            }
        }
    }

    public void cover(Set set) {
        ArrayList<SingleSet> sets = set.getSingleSetList();
        for (SingleSet singleSet : sets) {
            coverWithSingleSet(singleSet, covered);
        }
    }

    public int howManyWouldCover(Set set) {
        boolean[] hold = Arrays.copyOf(covered, covered.length);
        ArrayList<SingleSet> sets = set.getSingleSetList();
        for (SingleSet singleSet : sets) {
            coverWithSingleSet(singleSet, covered);
        }
        int sum = Operations.calculateBooleanSum(covered) - Operations.calculateBooleanSum(hold);
        covered = hold;
        return sum;
    }
}
