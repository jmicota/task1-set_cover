package cover;
//Justyna Micota 418427

// class representing a set that is a singleton, infinite or finite set, and that is
// later used as a component for Set
public class SingleSet {

    private final int begin;

    public SingleSet(int x) {
        this.begin = x;
    }

    public SingleSet copySingleSet() { return new SingleSet(begin); }

    public int getBegin() {
        return begin;
    }

    public int getDifference() {
        return -1;
    }

    public int getEnd() { return begin; }

    public int nrOfElements() {
        if (begin == 0) {return 0;}
        return 1;
    }
}
