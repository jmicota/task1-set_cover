package cover;
//Justyna Micota 418427

import java.util.ArrayList;

// class implementing greedy algorythm
public class Greedy {

    // decides which index in family of sets represents set that covers the
    // greatest number of not covered yet elements
    private static int indexOfSetToTake(ArrayList<Set> sets, Question question, int[] solution) {
        int maxNumberOfCovered = 0;
        int nrOfChosenSet = -1;
        for (int i = 0; i < sets.size(); i++) {
            Set set = sets.get(i);
            if (!Operations.appearsInTab(i + 1, solution)) {
                int nrOfCoveredElements = question.howManyWouldCover(set);
                if (nrOfCoveredElements > maxNumberOfCovered) {
                    nrOfChosenSet = i;
                    maxNumberOfCovered = nrOfCoveredElements;
                }
            }
        }
        return nrOfChosenSet;
    }

    public static int[] solution(int bound, ArrayList<Set> sets) {
        Question question = new Question(bound);
        int[] solutionTab = new int[0];

        int indexInSolutionTab = 0;
        int nrOfSet = -2;
        while (nrOfSet != -1 && !question.isCovered()) {
            nrOfSet = indexOfSetToTake(sets, question, solutionTab);
            if (nrOfSet >= 0) {
                Set set = sets.get(nrOfSet);
                solutionTab = Operations.addToTab(nrOfSet + 1, indexInSolutionTab, solutionTab);
                indexInSolutionTab++;
                question.cover(set);
            }
        }
        if (question.isCovered())
            return solutionTab;
        else return new int[0];
    }
}
