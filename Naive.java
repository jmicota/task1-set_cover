package cover;
//Justyna Micota 418427

import java.util.ArrayList;

// class implementing naive algorythm
public class Naive {

    private static boolean decideIfTakeSet(Set set, Question question) {
        if (question.howManyWouldCover(set) > 0) {
            return true;
        }
        return false;
    }

    public static int[] solution(int bound, ArrayList<Set> sets) {
        Question question = new Question(bound);
        int[] solutionTab = new int[0];

        int indexInSolutionTab = 0;
        int i = 0;
        while(i < sets.size() && !question.isCovered()) {
            if (decideIfTakeSet(sets.get(i), question)) {
                solutionTab = Operations.addToTab(i + 1, indexInSolutionTab, solutionTab);
                indexInSolutionTab++;
                question.cover(sets.get(i));
            }
            i++;
        }
        if (question.isCovered())
            return solutionTab;
        else return new int[0];
    }
}
