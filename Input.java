package cover;
// Justyna Micota 418427

import java.util.Scanner;

// class implementing analysis of input
public class Input {
    private static int getInt(Scanner scannner) {
        if (scannner.hasNextInt()) {
            return scannner.nextInt();
        }
        else return 0;
    }

    // passes the input to method creating a set or method solving an asked question
    public static void processInput(FamilyOfSets family) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int x = getInt(scanner);
            if (x >= 0) {
                makeSet(x, family, scanner);
            } else {
                int algorythmNumber = scanner.nextInt();
                Operations.printSolution(family.solution(-x, algorythmNumber));
            }
        }
        scanner.close();
    }

    // analyzes the input in order to create a set consisting of
    // three subtypes of SingleSet
    private static void makeSet(int beginOfSet, FamilyOfSets family, Scanner scanner) {
        Set set = new Set(); // makes empty set
        while (beginOfSet != 0) {
            SingleSet smallSet = new SingleSet(0);

            int dif = 0, end = 0;
            dif = getInt(scanner);
            if (dif >= 0) {
                smallSet = new SingleSet(beginOfSet);
                beginOfSet = dif;
            }
            else {
                end = getInt(scanner);
                if (end >= 0) {
                    smallSet = new InfiniteSet(beginOfSet, -dif);
                    beginOfSet = end;
                } else {
                    if (-end >= beginOfSet) {
                        smallSet = new FiniteSet(beginOfSet, -dif, -end);
                    }
                    beginOfSet = getInt(scanner);
                }
            }
            set.insertSingleSet(smallSet);
        }
        family.insertSet(set);
    }
}
