package cover;
//Justyna Micota 418427

import java.util.ArrayList;

// class implementing accurate, but expensive algorythm
public class Accurate {

    // check if binary representation of solution does cover the whole set from a question
    private static boolean checkSolution(int[] solution, ArrayList<Set> family, Question question) {
        for (int i = 0; i < solution.length; i++) {
            if (solution[i] == 1) {
                question.cover(family.get(i));
            }
        }
        if (question.isCovered()) {
            question.resetCovered();
            return true;
        }
        else {
            question.resetCovered();
            return false;
        }
    }

    public static int[] solution(int bound, ArrayList<Set> sets) {
        Question question = new Question(bound);
        int[] solution = recursiveSolution(0, new int[sets.size()], sets, question);
        return generateAnswer(solution);
    }

    // generating an array of chosen set numbers based on the binary
    // representation of final decisions for each set
    private static int[] generateAnswer(int[] solution) {
        if (solution == null) {
            return new int[0];
        }
        int[] tab = new int[Operations.calculateSum(solution)];
        int index = 0;
        if (tab.length == 0) {
            return tab;
        }
        for (int i = 0; i < solution.length; i++) {
            if (solution[i] == 1) {
                tab[index] = i + 1;
                index++;
            }
        }
        return tab;
    }

    // generates binary representation of final solution for family of sets
    // withI/withoutI is generating a solution that uses current index or not
    private static int[] recursiveSolution(int i, int[] solution, ArrayList<Set> sets, Question question) {
        if (i == sets.size()) {
            if (checkSolution(solution, sets, question))
                return solution.clone();
            return null;
        }
        int[] withoutI = recursiveSolution(i + 1, solution, sets, question);
        solution[i] = 1;
        int[] withI = recursiveSolution(i + 1, solution, sets, question);
        solution[i] = 0;
        if (withoutI != null && withI != null) {
            if (Operations.calculateSum(withoutI) < Operations.calculateSum(withI))
                return withoutI;
            else
                return withI;
        }
        else if (withoutI != null)
            return withoutI;
        else if (withI != null)
            return withI;
        else
            return null;
    }
}
