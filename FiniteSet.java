package cover;
//Justyna Micota 418427

public class FiniteSet extends InfiniteSet{
    private final int end;

    public FiniteSet(int x, int y, int z) {
        super(x, y);
        this.end = z;
    }

    @Override
    public FiniteSet copySingleSet() {
        return new FiniteSet(super.getBegin(), super.getDifference(), end);
    }

    @Override
    public int getDifference() {
        return super.getDifference();
    }

    @Override
    public int getEnd() {
        return end;
    }

    @Override
    public int nrOfElements() {
        int i = 0;
        int begin = super.getBegin();
        int dif = super.getDifference();
        while(begin <= end) {
            i++;
            begin += dif;
        }
        return i;
    }
}
