package cover;
//Justyna Micota 418427

import java.util.ArrayList;
import java.util.Arrays;

public class FamilyOfSets {
    private final ArrayList<Set> sets;

    public FamilyOfSets() {
        sets = new ArrayList<>();
    }

    private ArrayList<Set> copyArrOfSets() {
        ArrayList<Set> newList = new ArrayList<>();
        for (Set set : sets) {
            newList.add(set.copySet());
        }
        return newList;
    }

    public void insertSet(Set set) {
        sets.add(set);
    }

    // method choosing the algorythm based on the given algorythm number (1, 2, 3)
    public int[] solution(int setBound, int numberOfSolution) {
        int[] tab = new int[0];
        switch(numberOfSolution) {
            case 1:
                tab = Accurate.solution(setBound, copyArrOfSets());
                break;
            case 2:
                tab = Greedy.solution(setBound, copyArrOfSets());
                break;
            case 3:
                tab = Naive.solution(setBound, copyArrOfSets());
                break;
        }
        Arrays.sort(tab);
        return tab;
    }
}
