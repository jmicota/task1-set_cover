package cover;
//Justyna Micota 418427

import java.util.Arrays;

// class implementing basic operations on arrays
public class Operations {

    private static int[] resizeTab(int[] tab) {
        int[] tempTab = new int[tab.length * 2];
        System.arraycopy(tab, 0, tempTab, 0, tab.length);
        tab = new int[tempTab.length * 2];
        System.arraycopy(tempTab, 0, tab, 0, tempTab.length);
        return tab;
    }

    public static int[] addToTab(int nrOfSet, int index, int[] solutionTab) {
        if (solutionTab.length == 0) {
            solutionTab = new int[1];
        }
        if (index >= solutionTab.length) {
            solutionTab = resizeTab(solutionTab);
        }
        solutionTab[index] = nrOfSet;
        return solutionTab;
    }

    // calculates how many times in binary tab appears 1
    public static int calculateSum(int[] tab) {
        int sum = 0;
        for (int value : tab) {
            if (value == 1) {
                sum++;
            }
        }
        return sum;
    }

    // calculates how many times in tab appears value 'true'
    public static int calculateBooleanSum(boolean[] tab) {
        int sum = 0;
        for (boolean value : tab) {
            if (value) {
                sum++;
            }
        }
        return sum;
    }

    public static boolean appearsInTab(int x, int[] tab) {
        for (int value : tab) {
            if (value == x) {
                return true;
            }
        }
        return false;
    }

    // print the solution of a problem on screen
    public static void printSolution(int[] tab) {
        Arrays.sort(tab);
        if (tab.length == 0) {
            System.out.print("0\n");
        }
        else {
            int i = 0;
            // skipping zeros
            while (i < tab.length && tab[i] == 0) {
                i++;
            }
            // printing without last element
            for (int j = i; j < tab.length - 1; j++) {
                System.out.printf("%d ", tab[j]);
            }
            // printing last element with \n no matter if its zero or not
            System.out.printf("%d\n", tab[tab.length - 1]);
        }
    }
}
