package cover;
//Justyna Micota 418427

public class InfiniteSet extends SingleSet{
    private final int difference;

    public InfiniteSet(int x, int y) {
        super(x);
        this.difference = y;
    }

    @Override
    public InfiniteSet copySingleSet() {
        return new InfiniteSet(super.getBegin(), difference);
    }

    @Override
    public int getDifference() {
        return difference;
    }

    @Override
    public int getEnd() {
        return -1;
    }

    @Override
    public int nrOfElements() {
        return -1;
    }
}
