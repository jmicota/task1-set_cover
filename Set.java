package cover;
//Justyna Micota 418427

import java.util.ArrayList;

public class Set {
    private final ArrayList<SingleSet> singleSetList;

    public Set() {
        singleSetList = new ArrayList<>();
    }

    public Set(ArrayList<SingleSet> singleSetList) {
        this.singleSetList = singleSetList;
    }

    public Set copySet() {
        ArrayList<SingleSet> listOfSingleSets = new ArrayList<>();
        for (SingleSet singleSet : singleSetList) {
            listOfSingleSets.add(singleSet.copySingleSet());
        }
        return new Set(listOfSingleSets);
    }

    public ArrayList<SingleSet> getSingleSetList() {
        return singleSetList;
    }

    // checks if there is already a component of a given form in Set
    // doesnt add one if it already exists
    private boolean alreadyExistsInSet(SingleSet singleSet) {
        for (int i = 0; i < singleSetList.size(); i++) {
            SingleSet set = singleSetList.get(i);
            if (singleSet.nrOfElements() == 1 &&
                    set.nrOfElements() == 1 &&
                    singleSet.getBegin() == set.getBegin()) {
                return true;
            }
            else if (singleSet.getBegin() == set.getBegin() &&
                    singleSet.nrOfElements() == set.nrOfElements() &&
                    singleSet.getDifference() == set.getDifference()) {
                return true;
            }
        }
        return false;
    }

    public void insertSingleSet(SingleSet singleSet) {
        if (singleSet.nrOfElements() != 0) { // not empty somehow
            if (!alreadyExistsInSet(singleSet)) {
                singleSetList.add(singleSet);
            }
        }
    }
}
